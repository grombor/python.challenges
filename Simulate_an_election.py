import random

def chance_of_winning(probability_of_winning):
    if random.random() < probability_of_winning:
        return 1
    else:
        return 0
    
runs_num = 10_000
region_1_chance_of_winning = 0.87
region_2_chance_of_winning = 0.65
region_3_chance_of_winning = 0.17
total_win = 0

for i in range(runs_num):
    region_1 = chance_of_winning(region_1_chance_of_winning)
    region_2 = chance_of_winning(region_2_chance_of_winning)
    region_3 = chance_of_winning(region_3_chance_of_winning)

    total_chance = region_1 + region_2 + region_3

    if total_chance >= 2:
        total_win = total_win + 1

    chance_to_win = total_win / runs_num * 100 

print(f"The percentage of where Candidate A wins is {chance_to_win}")
        
    
