import random

capitals_dict = {
    'Alabama': 'Montgomery',
    'Alaska': 'Juneau',
    'Arizona': 'Phoenix',
    'Arkansas': 'Little Rock',
    'California': 'Sacramento',
    'Colorado': 'Denver',
    'Connecticut': 'Hartford',
    'Delaware': 'Dover',
    'Florida': 'Tallahassee',
    'Georgia': 'Atlanta',
    }


state_list = random.choice(list(capitals_dict))
capital_name = capitals_dict[state_list]


while True:
    question = input(f"What is the capital of the {state_list} ?  ").upper()
    if question == capital_name.upper():
        print("Correct")
        break
    elif question == "EXIT":
        print(f"The correct answer is {capital_name}, Goodbye")
        break
