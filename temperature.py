def convert_cel_to_far(C):
    """Convert temperatures from Celsius to Fahrenheit"""
    F = C * 9/5 + 32
    return print(f"{C}°C = {F}°F")

C = input(f"Enter a temperature in °C: ")
F = convert_cel_to_far(float(C))


def convert_far_to_cel(F):
    """Convert temperatures from Fahrenheit to Celsius"""
    C = (F - 32) * 5/9
    return print(f"{F}°F = {C}°C")

F = input(f"Enter a temperature in °F: ")
C = convert_far_to_cel(float(F))





