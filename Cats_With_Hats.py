cats = []

for lap in range(1, 101):
    for cat in range(1, 101):
        if cat % lap == 0:
            if cat in cats:
                cats.remove(cat)
            else:
                cats.append(cat)

print(cats)
